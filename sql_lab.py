# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
@author:Azadeh
"""

import os
import mysql.connector  as myc
from ScrapeClass import Scrape
from file import FILEIO
import requests        
"""this class Scrapes and cleans the data in coronavirus website and makes it ready to be saved in sql-Azadeh"""
"""this class saves the data created in scrape class in mysql table corona_table(Azadeh)"""

class Sqlpart:
    def __init__(self,host,user,password):
        self.host=host
        self.user=user
        self.password=password
        self.conn =self.connection_db(host,user,password)
        
    
   
    def connection_db (self,host,usr,passwd):
        try:
            return myc.connect(host=host,user=usr,password=passwd)
        except myc.error as err:
            print('some problem here with db connection{}'.format(err) )
    
    
    def create_db(self,crsr,db_name):
        crsr.execute('Create Database '+db_name)
    def select_db(self,crs,db_name):
        crs.execute('use '+db_name)
    def create_table(self,crs,table_name ,schema):
       crs.execute('create table '+table_name+ schema+';')
    def select_data_from_table(self,crs,table_name, columns_str, criteria_str):
       crs.execute('select '+columns_str+' from '+table_name+' where '+criteria_str +';')
    def populate_table(self,crs , table_name,lst_field,lst_value,format_str ):
        insert_stm=('insert into '+table_name + lst_field +' values '+format_str)
        crs.executemany(insert_stm,lst_value)
        crs.fetchall()
    """it creates the corona_table in covid_corona_db_ahma_zhan data base using the finallist from scrape class object"""
        
    def  create_db_tables_keys(self,url1,url2,x):
        crs=self.conn.cursor()
        self.create_db(crs,'covid_corona_db_ahma_zhan')
        self.select_db(crs,'covid_corona_db_ahma_zhan')
        schema2='(Scrapedate date  NOT NULL ,ranking  int(5) ,\
        Country_Other varchar(50) NOT NULL,Total_Case int(15) ,\
        New_Cases int(15) , Total_Death int(15), New_Deaths int(15) ,Total_Recovered int(15) ,New_Recovered int(15),Active_cases int(15),Serious_Critical int(15),\
        Tot_Cases_1M_pop int(15),Deaths_1M_pop int(15), Total_Tests int(15), Tests_1m_pop int(15),Population int(15),PRIMARY KEY (Scrapedate ,Country_Other));'
        self.create_table(crs,'corona_table ',schema2)
        lst_field1='(Scrapedate , ranking,Country_Other ,Total_Case,New_Cases , Total_Death , New_Deaths  ,Total_Recovered  ,New_Recovered,Active_Cases ,Serious_Critical ,Tot_Cases_1M_pop ,Deaths_1M_pop, Total_Tests, Tests_1m_pop ,Population )'
        date1="'2021-03-"+x+"'"
        x=str(int(x)-1)
        date2="'2021-03-"+x+"'"
        x=str(int(x)-1)
        date3="'2021-03-"+x+"'"
        x=str(int(x)+5)
        date4="'2021-03-"+x+"'"
        x=str(int(x)-1)
        date5="'2021-03-"+x+"'"
        x=str(int(x)-1)
        date6="'2021-03-"+x+"'"
        scrapeobj=Scrape(url1)
        webscrape=scrapeobj.bsomaker()
    
        finallis=scrapeobj.rows(webscrape,'main_table_countries_today')
        scrapeobj.tuplerow(finallis)
        format_str1='({},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'.format(date1)
        tuple1=scrapeobj.finallist
        self.populate_table(crs , 'corona_table  ',lst_field1,tuple1,format_str1 )
        self.conn.commit()
        
        scrapeobj0=Scrape(url1)
        webscrape0=scrapeobj0.bsomaker()
        finallis2=scrapeobj0.rows(webscrape0,'main_table_countries_yesterday')
        scrapeobj0.tuplerow(finallis2)
        tuple2=scrapeobj.finallist
        format_Str2='({},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'.format(date2)
        self.populate_table(crs , 'corona_table  ',lst_field1,tuple2,format_Str2 )
        self.conn.commit()
        
        scrapeobj1=Scrape(url1)
        webscrape1=scrapeobj1.bsomaker()
        finallis3=scrapeobj1.rows(webscrape1,'main_table_countries_yesterday2')
        scrapeobj1.tuplerow(finallis3)
        tuple3=scrapeobj1.finallist
        format_Str3='({},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'.format(date3)
        self.populate_table(crs , 'corona_table  ',lst_field1,tuple3,format_Str3 )
        self.conn.commit()
        
        scrapeobj2=Scrape(url2)
        webscrape2=scrapeobj2.bsomaker()
        finallis4=scrapeobj2.rows(webscrape2,'main_table_countries_yesterday')
        scrapeobj2.tuplerow(finallis4)
        tuple4=scrapeobj2.finallist
        format_str4='({},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'.format(date4)
        self.populate_table(crs , 'corona_table  ',lst_field1,tuple4,format_str4 )
        self.conn.commit()
        
        scrapeobj3=Scrape(url2)
        webscrape3=scrapeobj3.bsomaker()
        finallis5=scrapeobj3.rows(webscrape3,'main_table_countries_yesterday')
        scrapeobj3.tuplerow(finallis5)
        tuple5=scrapeobj3.finallist
        format_Str5='({},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'.format(date5)
        self.populate_table(crs , 'corona_table  ',lst_field1,tuple5,format_Str5 )
        self.conn.commit()
        
        scrapeobj4=Scrape(url2)
        webscrape4=scrapeobj4.bsomaker()
        finallis6=scrapeobj4.rows(webscrape4,'main_table_countries_yesterday2')
        scrapeobj4.tuplerow(finallis6)
        tuple6=scrapeobj4.finallist
        format_Str6='({},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'.format(date6)
        self.populate_table(crs , 'corona_table  ',lst_field1,tuple6,format_Str6 )
        self.conn.commit()
        
        self.conn.close()
    """thia method create and populates the countrie
    s_border table using FILEIO class pop attribute list -Azadeh"""    
       
    def colomnmaker(self,x):
        
         
        crs=self.conn.cursor()
        self.select_db(crs,'covid_corona_db_ahma_zhan')
        schema3="(Country_Other varchar(50),Distance Decimal NOT NULL ,Neigbour varchar(50) COLLATE Latin1_General_CS NOT NULL ,PRIMARY KEY(Country_Other,Neigbour))"
        name="countries_border"
        self.create_table(crs,name,schema3 )
        
        for i in range(0,len(x)):
            crs.execute("insert into countries_border(Country_Other,Neigbour,Distance) values "+str(x[i])+";")
            self.conn.commit()
        self.conn.close()
       
    
              
       
              
             
           
           
           
"""it is the main method that asks 
the user to enter anumber and based on 
that scrapes   html file or saves on the local web file 
calling   the above methodAzadeh _Guang"""          
           
def main_scrape_populate_tables():
    response=int(input("""
     please enter your choice
     enter 1 if you want to save as local web file 
     enter 2 if you want to scrape from locally saved page
     """))                 
    if response == 1:
        url = 'https://www.worldometers.info/coronavirus/'
        filename = input("Enter html name you want to save as:")
        
       
        save_localhtml(url, filename)
    elif response==2:        
    
        x=input("please enter the number of day") 
        path=os.path.join(os.getcwd(),"local_html")
        filename1=os.path.join(path,"local_page2021-03-"+x)
        filename2=os.path.join(path,"local_page2021-03-"+str(int(x)+3))
      
                               
        url1="file:///"+filename1+".html"
        url2="file:///"+filename2+".html"
        user =os.environ['admin']
        passwd=os.environ['password']
        host='localhost'
        sobj=Sqlpart(host,user,passwd)
         
        js=FILEIO("country_neighbour_dist_file.json") 
        js.readFromFile() 
        js.readvalue()          
        js.populate()           
                  

        sobj.create_db_tables_keys(url1,url2,x)       
        sobj.colomnmaker(js.pop)        

             
def save_localhtml(url, htmlname):
        response = requests.get(url)
        html = response.content
        html_path = os.path.join(os.getcwd(), 'local_html')
        html_name = os.path.join(html_path, htmlname)
        f = open(html_name, 'wb')
        f.write(html)
        f.close
                 
    
       
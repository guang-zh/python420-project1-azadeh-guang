# python420-project1-azadeh-guang

Python420 Project 1 Web Scrapping for Coronavirus cases

## Driver classes:
- mainAZ: User chooses Scrapping or DataScience
- mainGZ: 14-days trends. User chooses Saving html from web, Scrapping from local html, or DataScience

## Scrapping, MySQL, DataScience (Azadeh):
- ScrapeClass: scrapping from local html
- sql_lab: connection to MySQL database and populate tables
- data_science: make plots from SQL tables

## Scrapping, DataScience (Guang): 
- scrape_module: scrapping from URL and local html, save to df, CSV
- data_explore_module: make plots for the country User interested in
- (deleted) json_to_df, sqllab_gz: sql tests

### modules are shared and used in different classes between us.

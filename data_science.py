# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 10:48:21 2021

@author: Admin
"""
import json
import os
import matplotlib
import pandas as pd
from bs4 import BeautifulSoup
from urllib.request import urlopen
from sql_lab import Sqlpart
def main_dataScience_Xplore():
    user =os.environ['admin']
    passwd=os.environ['password']
    host='localhost'
    sobj=Sqlpart(host,user,passwd)
    crs=sobj.conn.cursor()
    sobj.select_db(crs,'covid_corona_db_ahma_zhan')
    country =input('enter a country')
    query="select Scrapedate,New_Cases,New_Deaths,New_Recovered  from corona_table where Country_Other={};".format("'"+country+"'")
    
    my_df = pd.read_sql_query(query, sobj.conn)
    my_df.plot(x ="Scrapedate", y=["New_Cases","New_Deaths","New_Recovered"], kind = 'bar')
    query2="select New_Cases from corona_table where Country_Other=(select Neigbour from countries_border where Country_Other={} and Distance =(select max(Distance) from  countries_border  where Country_Other={} group by Country_Other));".format("'"+country+"'","'"+country+"'") 
    crs.execute("select Neigbour from countries_border where Country_Other={} and Distance =(select max(Distance) from  countries_border  where Country_Other={} group by Country_Other);".format("'"+country+"'","'"+country+"'"))
    name=crs.fetchall()                                                                                                                                                                                                                          
    df2=pd.read_sql_query(query2, sobj.conn)
    df2=df2.rename(columns={"New_Cases":name[0][0]+' New cases'})
    query3="select Scrapedate,New_Cases as {}_Newcases from corona_table where Country_Other={};".format(country,"'"+country+"'")
    df3=pd.read_sql_query(query3, sobj.conn)
    finaldf=pd.concat([df2,df3],axis=1)
    finaldf.plot(x ="Scrapedate", y=["{}_Newcases".format(country),"{}".format(name[0][0]+" New cases")], kind = 'bar')
    crs.execute("select Neigbour from countries_border where Country_Other={} order by Distance DESC;".format("'"+country+"'"))
    countries=crs.fetchall()
    query7="select Scrapedate,Deaths_1M_pop as {}_Deaths_1M_pop from corona_table where Country_Other={};".format(country,"'"+country+"'")
    df7=pd.read_sql_query(query7, sobj.conn)

    query4="select Deaths_1M_pop as {}_Deaths_1M_pop from corona_table where Country_Other={};".format(countries[0][0],"'"+countries[0][0]+"'")
    df4=pd.read_sql_query(query4, sobj.conn)
    dffinal=pd.concat([df7,df4],axis=1)
    query5="select Deaths_1M_pop as {}_Deaths_1M_pop from corona_table where Country_Other={};".format(countries[1][0],"'"+countries[1][0]+"'")
    df5=pd.read_sql_query(query5, sobj.conn)
    dffinal2=pd.concat([dffinal,df5],axis=1)
    dffinal2.plot(x ="Scrapedate", y=["{}_Deaths_1M_pop".format(country),"{}_Deaths_1M_pop".format(countries[0][0]),"{}_Deaths_1M_pop".format(countries[1][0])], kind = 'bar')     
   